function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Given a transport picks up units.",
		parameterDefs = {
			{ 
				name = "transport",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGetUnitVelocity = Spring.GetUnitVelocity
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting

-- @description return `table`'s values in a new table where they function as keys
function valueSet(table)
	local set = {}
	for _, v in ipairs(table) do set[v] = true end
	return set
end

-- @description return a unit from `units` that is not in `transporting`
function nextToPick(transporting, units)
	for _, unitID in ipairs(units) do
		if not transporting[unitID] then
			return unitID
		end
	end

	return nil
end

local function ClearState(self)
	self.pickingUID = nil
end

function Run(self, unitIds, p)
	local transporting = valueSet(SpringGetUnitIsTransporting(p.transport))

	-- check if unit has been picked
	if transporting[self.pickingUID] then
		self.pickingUID = nil
		return RUNNING
	end

	-- check if we are currently picking up some unit
	if self.pickingUID ~= nil then
		local _, _, _, vel = SpringGetUnitVelocity(p.transport)
		if vel == 0 then
			SpringGiveOrderToUnit(
				p.transport,
				CMD.LOAD_UNITS,
				{self.pickingUID},
				{}
			)
		end

		return RUNNING
	end

	-- get which unit to pick next
	self.pickingUID = nextToPick(transporting, p.units)

	if self.pickingUID == nil then
		-- there is no unit to pick
		return SUCCESS
	else
		-- there is unit to pick
		SpringGiveOrderToUnit(
			p.transport,
			CMD.LOAD_UNITS,
			{self.pickingUID},
			{}
		)
		return RUNNING
	end
end

function Reset(self)
	ClearState(self)
end
