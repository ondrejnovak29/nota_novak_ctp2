function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Orders a transport unit to unload on a given location.",
		parameterDefs = {
			{ 
				name = "transport",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "location",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting

local function ClearState(self)
	self.moving = false
end

function Run(self, unitIds, p)
    if not self.moving then
        SpringGiveOrderToUnit(
            p.transport,
            CMD.UNLOAD_UNITS,
            {p.location.x, p.location.y, p.location.z, 25},
            {}
        )
        self.moving = true
	end

	if #SpringGetUnitIsTransporting(p.transport) == 0 then
		return SUCCESS
	else
		return RUNNING
	end
end

function Reset(self)
	ClearState(self)
end
