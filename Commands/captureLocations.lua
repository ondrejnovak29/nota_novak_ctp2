function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Given a set of units and locations, splits units and moves them onto the locations.",
		parameterDefs = {
			{ 
				name = "units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "locations",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

local function ClearState(self)
	self.moving = false
end

function Run(self, unitIds, p)
    if not self.moving then
        for i = 1, #p.units do
            local loc = p.locations[i % #p.locations + 1]
            local height = SpringGetGroundHeight(loc.x, loc.z)
            SpringGiveOrderToUnit(
                p.units[i],
                CMD.MOVE,
                {loc.x, height, loc.z},
                {}
            )
        end
        self.moving = true
	end

    local allReached = true
    for i = 1, #p.units do
		local unitX, _, unitZ = SpringGetUnitPosition(p.units[i])
        local loc = p.locations[i % #p.locations + 1]
		if euclDist(unitX, unitZ, loc.x, loc.z) > 100 then
            allReached = false
        end
    end

	if allReached then
		return SUCCESS
	else
		return RUNNING
	end
end

function Reset(self)
	ClearState(self)
end
