local sensorInfo = {
	name = "gameShouldEnd",
	desc = "Returns true if bonus points have been reached or time is about to run out.",
	author = "Ondřej Novák",
	date = "2020-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description converts time representation to seconds
function timeToSec(t)
    return 3600 * t.h + 60 * t.m + t.s
end

-- @description returns whether we should end the current mission
return function(info)
    return info.score >= info.scoreForBonus or timeToSec(info.remainingTime) < 5
end
