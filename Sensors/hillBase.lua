local sensorInfo = {
	name = "hillBase",
	desc = "Return position at the base of the hill.",
	author = "Ondřej Novák",
	date = "2020-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

-- @description return position 250 units west of given position
return function(hill)
    local x, z = hill.x - 250, hill.z
    local y = SpringGetGroundHeight(x, z)

    return {x=x, y=y, z=z}
end
