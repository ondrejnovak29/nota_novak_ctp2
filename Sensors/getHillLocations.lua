local sensorInfo = {
	name = "getHillLocations",
	desc = "Return positions of guarded and unguarded hills.",
	author = "Ondřej Novák",
	date = "2020-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 100000

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetMetalMapSize = Spring.GetMetalMapSize
local SpringGetGroundHeight = Spring.GetGroundHeight

-- @description given a point on a rectangular platform, return its center 
function getCenter(height, x, z)
	local tl = { x=x, z=z }
	while SpringGetGroundHeight(tl.x - 1, tl.z) == height do tl.x = tl.x - 1 end
	while SpringGetGroundHeight(tl.x, tl.z - 1) == height do tl.z = tl.z - 1 end

	local br = { x=x, z=z }
	while SpringGetGroundHeight(br.x + 1, br.z) == height do br.x = br.x + 1 end
	while SpringGetGroundHeight(br.x, br.z + 1) == height do br.z = br.z + 1 end

	return { x = (tl.x + br.x) / 2, z = (tl.z + br.z) / 2 }
end

-- @description horizontal euclidean distance between 2 points
function euclDist(x1, z1, x2, z2)
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(z1 - z2, 2))
end

-- @description return positions of guarded and unguarded hill
return function(info)

	local sizeX, sizeZ = SpringGetMetalMapSize()
	local stride = 128

	-- search map for hill locations
	local hills = {}
	for x=0, 512 * sizeX, stride do 
		for z=0, 512 * sizeZ, stride do 
			if SpringGetGroundHeight(x, z) == info.areaHeight then
 				table.insert(hills, getCenter(info.areaHeight, x, z))
			end
		end
    end
	
	-- split guarded and unguarded hills
    local unguarded, guarded = {}, {}
    for _, hill in pairs(hills) do
        local isGuarded = false
        for _, enemy in pairs(info.enemyPositions) do
            if euclDist(enemy.x, enemy.z, hill.x, hill.z) < stride then
                isGuarded = true
            end 
        end

        if isGuarded then
            table.insert(guarded, hill)
        else
            table.insert(unguarded, hill)
        end
    end

	return {
		unguarded = unguarded,
		guarded = guarded
	}
end
