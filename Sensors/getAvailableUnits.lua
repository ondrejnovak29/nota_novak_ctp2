local sensorInfo = {
	name = "getAvailableUnits",
	desc = "Return units split into 3 categories (light, heavy, transport).",
	author = "Ondřej Novák",
	date = "2020-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetTeamUnitsByDefs = Spring.GetTeamUnitsByDefs
local myTeamID = Spring.GetMyTeamID()

-- @description return my team's units
return function()
    return { 
        light = SpringGetTeamUnitsByDefs(myTeamID, 122), -- peewees
        heavy = SpringGetTeamUnitsByDefs(myTeamID, 162), -- warriors
        transport = SpringGetTeamUnitsByDefs(myTeamID, 149) -- bears
    }
end
